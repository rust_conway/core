#[cfg(test)]
use super::*;

#[allow(unused_imports)]
mod dimensions {
    use super::*;
    
    #[test]
    fn new() {
        assert_eq!(Dimensions::new(1, 1), Dimensions { height: 1, width: 1 })
    }

    #[test]
    fn square() {
        assert_eq!(Dimensions::new(10, 2).square(), 20);
    }

    #[test]
    fn partial_eq() {
        assert_eq!(Dimensions::new(3, 4), Dimensions::new(3, 4));
    }

}

#[allow(unused_imports)]
mod status {
    use super::*;

    #[test]
    fn from_i32() {
        assert_eq!(Status::Unpopulated, Status::from(0));
    }
}


#[allow(unused_imports)]
mod field {
    use super::*;

    #[test]
    fn new() {
        let size = Dimensions::new(2, 2);
        let field = vec![Status::Unpopulated, Status::Unpopulated, Status::Unpopulated, Status::Unpopulated];
        assert_eq!(Field::new(2, 2), Field { size, field });
    }

    #[test]
    fn from_status() {
        let mut x = Field::new(2, 2);
        x.field[2] = Status::Populated;
        let size = Dimensions::new(2, 2);
        let field = vec![Status::Unpopulated, Status::Unpopulated, Status::Populated, Status::Unpopulated];
        let y = Field::from_status(size, field);
        assert_eq!(x, y);
    }

    #[test]
    fn from_i32() {
        let x = Field::new(2, 2);
        let y = Field::from_i32( Dimensions::new(2, 2), vec![0, 0, 0, 0]);
        assert_eq!(x, y);
    }


    #[test] 
    fn partial_eq() {
        let x = Field::new(2, 2);
        let y = Field::new(2, 2);
        assert_eq!(x, y);
    }


    #[test]
    fn format() {
        let pop = String::from(POP_SYM);
        let non_pop = String::from(NON_POP_SYM);

        let x = format!("{}", Field::from_i32(Dimensions::new(2, 2), vec![0, 1, 1, 0]));
        let y = format!( "{}{}\n{}{}\n", non_pop, pop, pop, non_pop);
        assert_eq!(x, y);
    }

    #[test]
    fn iter_blinker() {
        let mut blinker = Field::from_i32(Dimensions::new(3, 3), vec![0, 1, 0, 0, 1, 0, 0, 1, 0]);
        let copy = blinker.clone();
        blinker.next_step();
        blinker.next_step();
        assert_eq!(copy, blinker);
    }
}
