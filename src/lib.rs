const POP_SYM: &str = "██";
const NON_POP_SYM: &str = "  ";
// •,█

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Status { Populated, Unpopulated }

#[derive(Clone, Debug, Copy)]
pub struct Dimensions {
    pub height: usize,
    pub width: usize
}

#[derive(Clone, Debug)]
pub struct Field {
    pub size: Dimensions,
    pub field: Vec<Status>,
}
//
//
//
//
//

impl std::convert::From<i32> for Status {
    fn from(val: i32) -> Self {
        if val <= 0 { Status::Unpopulated } else { Status::Populated }
    }
}

impl Dimensions {
    pub fn new(width: usize, height: usize) -> Dimensions {
        Dimensions { width, height }
    }

    pub fn square(&self) -> usize {
        self.height * self.width
    }
}

impl std::cmp::PartialEq for Dimensions {
    fn eq(&self, other: &Self) -> bool {
        self.width == other.width && self.height == other.height
    }
}

impl std::cmp::PartialEq for Field {
    fn eq(&self, other: &Field) -> bool {
        self.size == other.size && self.field.iter().zip(other.field.iter()).all(|(x, y)| *x == *y)
    }
}

impl std::fmt::Display for Field {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out = String::new();

        for (index, &element) in self.field.iter().enumerate() {
            // 1 -> populated,
            // 0 -> unpopulated
            let repr = if let Status::Populated = element { POP_SYM } else { NON_POP_SYM };

            let offset = index % self.size.width;

            if offset == self.size.width - 1 {
                out.push_str(&format!("{}\n", repr));
            } else {
                out.push_str(&format!("{}", repr));
            }
        }
        write!(f, "{}", out)
    }
}

impl Field {
    pub fn new(width: usize, height: usize) -> Field {
        let size = Dimensions::new(width, height);
        let dim = size.square();
        Field { size, field: vec![Status::Unpopulated; dim]}
    }

    pub fn from_status(size: Dimensions, field: Vec<Status>) -> Field {
        assert_eq!(size.square(), field.len());
        Field { size, field }
    }

    pub fn from_i32(size: Dimensions, field: Vec<i32>) -> Field {
        assert_eq!(size.square(), field.len());
        Field { size, field: field.iter().map(|x| Status::from(*x)).collect() }
    }

    pub fn fill(&mut self) {
        // fill the matrix with some values for initialisation
        for element in self.field.iter_mut() {
            *element = Status::Populated;
        }
    }

    pub fn next_step(&mut self) {
        // TODO: add check if stagnant

        // init neighbors count for calculations
        let mut total_neighbors = vec![0; self.size.square()];

        let coords = vec![-1, 0, 1];
        for index in 0..self.size.square() {
            for y in coords.iter() {
                for x in coords.iter() {
                    // diregard the actual cell, isnt neighbor to itself
                    if *x == 0 && *y == 0 { continue; }

                    let index_i32 = index as i32;
                    let width_i32 = self.size.width as i32;
                    let height_i32 = self.size.height as i32;

                    if (index_i32 % width_i32) + *x < 0 || (index_i32 % width_i32) + *x > width_i32- 1 { continue; }
                    if index_i32 + *y * width_i32 < 0  || index_i32 + *y * width_i32 > width_i32 * height_i32 { continue; }

                    let n_index = index_i32 + *x + width_i32 * *y;

                    if n_index >= 0 {
                        let n_index = n_index as usize;
                        if let Some(val) = self.field.get(n_index as usize) {
                            total_neighbors[index] += if let Status::Populated = val {1 } else {0};
                        }

                    }
                }
            }
        }

        // then apply the rules to self

        // Rules: 
        //
        // For a space that is 'populated':
        //     Each cell with one or no neighbors dies, as if by solitude. 
        //     Each cell with four or more neighbors dies, as if by overpopulation. 
        //     Each cell with two or three neighbors survives. 
        //
        // For a space that is 'empty' or 'unpopulated'
        //     Each cell with three neighbors becomes populated
        let new_field: Vec<Status> = self.field.iter().zip(total_neighbors.iter()).map(|x| {
            if let Status::Populated = x.0 {      // for fields that are poplated
                match x.1 {
                    0|1 => Status::Unpopulated,   // 0 or 1 dies
                    2|3 => Status::Populated,     // 2 or 3 lives on
                    _   => Status::Unpopulated    // 4 or more -> starvin marvin
                }
            } else {                              // for unpopulated fields
                match x.1 {
                    3   => Status::Populated,     // 3, new life!
                    _   => Status::Unpopulated    // otherwise dead
                }
            }
        }).collect();
        self.field = new_field;
    }
}
